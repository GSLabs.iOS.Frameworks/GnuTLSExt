//
//  GnuTLSExt.h
//  Pods
//
//  Created by Dan Kalinin on 12/30/20.
//

#import <GnuTLSExt/TlsMain.h>
#import <GnuTLSExt/TlsInit.h>

FOUNDATION_EXPORT double GnuTLSExtVersionNumber;
FOUNDATION_EXPORT const unsigned char GnuTLSExtVersionString[];
